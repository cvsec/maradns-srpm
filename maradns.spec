%global _hardened_build 1

%define version_major 3
%define version_minor 5
%define version_patch 0036

%define version_major_minor %{version_major}.%{version_minor}

Name:		maradns
Version:	%{version_major_minor}.%{version_patch}
Release:        1%{?dist}
Summary:	A small open-source DNS server

%define url_root https://maradns.samiam.org
%define url_download_common %{url_root}/download/%{version_major_minor}/%{version}/maradns-%{version}.tar.xz

License:	BSD-2-Clause
URL:		%{url_root}/
Source0:	%{url_download_common}
Source1:	%{url_download_common}.asc
Source2:	gpgkey.txt

BuildRequires:	gcc
BuildRequires:	gnupg2
BuildRequires:	make

%description
MaraDNS is a free open-source computer program written by Sam Trenholme.

MaraDNS implements the Domain Name System (DNS), an essential internet service. MaraDNS is open source software: This means that anyone is free to download, use, and modify the program free of charge, as per its license.

People like MaraDNS because it's small, lightweight, easy to set up, and remarkably secure. It's also cross platform -- the program runs both in Windows and in UNIX clones.

More information describing MaraDNS is on the overview and summary page.

%prep
gpg2 --quiet --no-default-keyring --keyring ./keyring --import %{SOURCE2}
gpg2 --quiet --no-default-keyring --keyring ./keyring --verify %{SOURCE1} %{SOURCE0}
%autosetup

%build
%configure
%make_build

%install
%make_install

%check

%files
%license
%doc

%changelog
* Wed Nov 29 2023 Random Person <cvsec2@protonmail.com>
- Update to maradns release 3.5.0036
